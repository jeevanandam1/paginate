import PaginatedItems from "./PaginatedItems";

import { useEffect, useState } from "react";

function App() {
	const [initial, setInitial] = useState([]);
	const [show, setShow] = useState(true);

	useEffect(() => {
		async function getData() {
			setShow(true);
			const response = await fetch("https://fakestoreapi.com/products");
			const data = await response.json();
			setInitial(data);
			setShow(false);
		}
		getData();
	}, []);

	return (
		<div className="App">
			<div className="w-11/12 mx-auto">
				{show ? (
					<div className="flex items-center justify-center min-h-screen">
						<p className="text-base font-bold lg:text-2xl text-slate-800">
							Fetching...
						</p>
					</div>
				) : (
					<PaginatedItems itemsPerPage={4} initial={initial} />
				)}
			</div>
		</div>
	);
}

export default App;
