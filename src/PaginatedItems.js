import React, { useEffect, useState } from "react";
import ReactPaginate from "react-paginate";

import styles from "./Paginate.module.css";

import Items from "./Items";

const PaginatedItems = ({ itemsPerPage, initial }) => {
	const [currentItems, setCurrentItems] = useState(null);
	const [pageCount, setPageCount] = useState(0);
	const [itemOffset, setItemOffset] = useState(0);

	useEffect(() => {
		const endOffset = itemOffset + itemsPerPage;
		setCurrentItems(initial.slice(itemOffset, endOffset));
		setPageCount(Math.ceil(initial.length / itemsPerPage));
	}, [itemOffset, itemsPerPage, initial]);

	const handlePageClick = (event) => {
		const newOffset = (event.selected * itemsPerPage) % initial.length;
		setItemOffset(newOffset);
	};

	return (
		<>
			<Items currentItems={currentItems} />
			<div className={`mx-auto my-4 w-11/12 lg:w-8/12 ${styles.paginate}`}>
				<ReactPaginate
					breakLabel="..."
					nextLabel="next >"
					onPageChange={handlePageClick}
					pageRangeDisplayed={5}
					pageCount={pageCount}
					previousLabel="< previous"
					renderOnZeroPageCount={null}
				/>
			</div>
		</>
	);
};

export default PaginatedItems;
