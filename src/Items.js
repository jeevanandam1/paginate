const Items = ({ currentItems }) => {
	return (
		// List Items
		<div className="flex flex-wrap justify-center gap-4 my-4 ">
			{currentItems &&
				currentItems.map((item) => (
					<div
						className="w-11/12 h-40 p-2 mx-4 overflow-y-hidden transition-all duration-75 border rounded md:w-96 hover:shadow-md"
						key={item.id}
					>
						<p className="my-2 text-sm font-bold text-red-400">
							Item #{item.id}
						</p>
						<p className="overflow-hidden text-base font-extrabold text-slate-800">
							{item.title}
						</p>
						<p className="my-2 overflow-hidden text-sm font-light capitalize text-slate-800">
							{item.description}
						</p>
					</div>
				))}
		</div>
	);
};

export default Items;
